package TextAnalizer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word {
    String word;
    Integer count;

    public Word (String word){
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getNumberOfLetters (String letters){
        Pattern pat = Pattern.compile(letters);
        Matcher m = pat.matcher(this.word);
        int count = 0;
        while (m.find()) count++;
        return count;
    }

    public void setCount(String letter) {
        this.count = this.getNumberOfLetters(letter);

    }

    public Integer getCount() {
        return count;
    }
}
