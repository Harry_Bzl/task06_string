package TextAnalizer;


import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
