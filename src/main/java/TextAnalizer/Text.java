package TextAnalizer;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Text {
    String text;
    public Text (String text){
        this.text = text.trim();
    }

    private static List<String> apply(String x) {
        Sentence sen = new Sentence(x);
        return sen.splitIntoWords();
    }

    /**
     * replace all vowels with underscores
     * @return text with changes
     */
    public String replacevowels (){
        String newText = text.replaceAll("[AEIOUYaeiouy]", "_");
        return newText;
    }

    public List<String> splitIntoSentemces (){
        List <String> sentList = new ArrayList<>();
        Pattern p = Pattern.compile("[^\\.\\!\\?]*[\\.\\!\\?]");
        Matcher m = p.matcher(text);
        while (m.find()) sentList.add(m.group());
        return sentList;
    }



    /**
     * find the biggest quantity of sentences with similar words
     * @return print word and number of sentences
     */
    public void printMaxFrequencyOfSimilarWords () {
        List <String> sentList = splitIntoSentemces();
        Set <String> words = new Sentence(text).getWordsWithoutDuplicates();
        HashMap <String,Integer> counts = new HashMap<>();

        for (String i: words){
            Pattern p = Pattern.compile("[\\s]" + i + "[\\s\\.\\!\\?]");
            for (String j: sentList){

                if (p.matcher(j).find()){
                    counts.put(i, counts.getOrDefault(i,0)+1);
                }
            }
        }
        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry :counts.entrySet()){
            if (maxEntry == null ||entry.getValue().compareTo(maxEntry.getValue()) > 0){
                maxEntry = entry;
            }
        }
        System.out.println("Number of sentences is " + sentList.size());
        System.out.println("Number of unique word is " + words.size());
        System.out.println("The most frequent word is '"+ maxEntry.getKey() + "' that is in "
                + maxEntry.getValue() + " sentences");
    }

    /**
     * divide text by sentences ant sort by word number
     * @return sorted list of sentences
     */
    public List <String> sortedByWordNumber () {
        List <String> result = splitIntoSentemces();
        result.sort((x,y)-> {
            Integer n = new Sentence ((String)x).splitIntoWords().size();
            Integer m = new Sentence ((String)y).splitIntoWords().size();
            return n.compareTo(m);
        });
        return result;
    }

    public void printSentences (List<String> sentences){
        for (String i : sentences)System.out.println(i);
    }

    /**
     * find unique word in first sentence
     */
    public void findUniqueWord (){
        List <String> sentences = splitIntoSentemces();
        Sentence first = new Sentence(sentences.get(0));
        List <String> wordsFirst = first.splitIntoWords();
        boolean find = false;
        String word = "";
        for (int i = 0; i<wordsFirst.size(); i++){
            Pattern pat = Pattern.compile("\\s" + wordsFirst.get(i)+ "[\\s\\.\\!\\?]");
            for (int j = 1; j<sentences.size(); j++){
                Matcher m = pat.matcher(sentences.get(j));
                if (!m.find()){
                    find = true;
                    word = wordsFirst.get(i);
                    break;
                }
            }
        }
        if (find) System.out.println("Unique word is " + word);
        else System.out.println("No unique word!");
    }

    /**
     * print words of spetial size in questions
     * @param size size of printed words
     */
    public void printWordInQuestion (int size){
        Pattern pat = Pattern.compile("^\\s.*\\?$");

        Set <String> sentences = splitIntoSentemces().stream().filter(x-> {
                Matcher m = pat.matcher(x);
                return m.matches();
        }).flatMap(i-> {
            Sentence sen = new Sentence(i);
            return sen.splitIntoWords().stream();
        }).filter(w->w.length() ==size).collect(Collectors.toSet());
        System.out.println("Words with size " + size + " are: ");
        for (String i : sentences) System.out.println(i);
    }

    /**
     * replase all words started with vowel to max size word
     * @return list of sentences
     */
    public List<String> changeVowelWord (){
        List <String> result = new ArrayList<>();
        for (String sent : this.splitIntoSentemces()){
            List <String> words = new Sentence(sent).splitIntoWords();
            String max  = words.stream().max(Comparator.comparing(String::length)).get();
            String resSent = sent.replaceFirst("\\b[eyuoiaEYUIOA]\\w*", max);
            result.add(resSent);
        }

        return result;
    }
}
