package TextAnalizer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ConsoleEvents {
    private Scanner input = new Scanner(System.in);
    private Map<String, String> langMenu;
    private Map<String,Printable> methodsMenu;
    private Locale locale;
    private ResourceBundle bundle;

    String getAnalizedText() {
        return analizedText;
    }

    private String analizedText;

    ConsoleEvents(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("FirstMenu", locale);
        setLangMenu ();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::internationalizeMenuEnglish);
        methodsMenu.put("2", this::internationalizeMenuUkrainian);
        methodsMenu.put("3", this::internationalizeMenuFrench);
        methodsMenu.put("4", this::internationalizeMenuChinese);
        methodsMenu.put("5", this::analizeMenu);
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("FirstMenu", locale);
        setLangMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("FirstMenu", locale);
        setLangMenu();
        show();
    }

    private void internationalizeMenuFrench () {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("FirstMenu", locale);
        setLangMenu();
        show();
    }

    private void internationalizeMenuChinese () {
        locale = new Locale("zh");
        bundle = ResourceBundle.getBundle("FirstMenu", locale);
        setLangMenu();
        show();
    }

    private void setLangMenu (){
        langMenu = new HashMap<>();
        langMenu.put("1", bundle.getString("1"));
        langMenu.put("2", bundle.getString("2"));
        langMenu.put("3", bundle.getString("3"));
        langMenu.put("4", bundle.getString("4"));
        langMenu.put("5", bundle.getString("5"));

    }

    void show() {
        String keyMenu;
        do {
            selectMenu();
            System.out.println(bundle.getString("6"));
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!langMenu.keySet().contains(keyMenu));
    }

    private void selectMenu() {
        System.out.println("MENU:");
        for (String key : langMenu.keySet()) {
            if (key.length() == 1) {
                System.out.println(langMenu.get(key));
            }
        }
    }


    private String startMenu() {
        String answer;
        do {
            System.out.println(bundle.getString("7"));
            System.out.println("Sentence  |   File");
            answer = input.nextLine();
        }
        while (!answer.equalsIgnoreCase("sentence") && !answer.equalsIgnoreCase("file"));
        return answer;
    }

    private void analizeMenu() throws IOException {
        String startAnsver = startMenu();
        if (startAnsver.equalsIgnoreCase("sentence")){
            analizedText = typeMenu();
        }
        else analizedText = chooseFileMenu();
    }

    private String typeMenu() {
        System.out.println(bundle.getString("8") );
        String ansver = input.nextLine();
        if (!ansver.isEmpty())return ansver;
        else typeMenu();
        return ansver;
    }

    private String chooseFileMenu() throws IOException {
//        System.out.println("Sentence  |   File");
        System.out.println(bundle.getString("9")) ;
        File [] files = getFiles();
        String text;
        for (int i = 1; i<= files.length;i++){
            System.out.println(i + " --- " + files [i-1].getName());
        }
        int fileNumber = 0;
        do {
            System.out.println(bundle.getString("10") );
            try {
                fileNumber = input.nextInt();
                System.out.println(
                        "The number entered is " + fileNumber);
            } catch(InputMismatchException ex){
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                chooseFileMenu();
            }
        } while (0 > fileNumber && fileNumber >= files.length);

        try (Stream<String> stream = Files.lines(Paths.get(files[fileNumber-1].getPath()))) {
            //stream.forEach(System.out::println);
            text = stream.collect(Collectors.joining(" "));
        }

        return text;
    }

    private File [] getFiles() {
        File folder = new File("src\\main\\resources\\texts");
        return folder.listFiles();
    }
}
