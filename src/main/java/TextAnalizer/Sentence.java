package TextAnalizer;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static javafx.scene.input.KeyCode.T;

public class Sentence {
    String sentence;

    public Sentence (String sentence){
        this.sentence = sentence;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence.trim();
    }

    /**
     * check if sentence begin with capital letter and end with period.
     * @return boolean
     */
    public boolean checkFirstUpperLastPeriod(){
        Pattern p = Pattern.compile("^[A-Z].*\\.$");
        Matcher matcher = p.matcher(sentence);
        if (matcher.find()){
            System.out.println("Sentence begin with capital letter and end with period.");
            return true;
        }
        else {
            System.out.println("Sentence NOT begin with capital letter OR end with period.");
            return false;
        }
    }

    public List<String> splitIntoWords (){
        List <String> wordList = new ArrayList<>();
        Pattern p = Pattern.compile("\\w+");
        Matcher m = p.matcher(sentence);
        while (m.find()) wordList.add(m.group());
        return wordList;
    }

    public Set<String> getWordsWithoutDuplicates (){
        Set <String> words = new HashSet<>();
        for (String i : splitIntoWords()){
            words.add(i);
        }
        return words;
    }

    /**
     * print wordsn order. Start new line with new letter
     */

    public void printWordsInOrder () {
        Set <String> words = this.getWordsWithoutDuplicates();
        List <String> orderedWords = words.stream().sorted().collect(Collectors.toList());
        System.out.println("Words of text in order:");
        System.out.print(orderedWords.get(0));
        for (int i = 1; i<orderedWords.size(); i++){
            if (orderedWords.get(i).charAt(0)!= orderedWords.get(i-1).charAt(0)){
                System.out.println();
            }
            System.out.print(" " + orderedWords.get(i));
        }
    }

    /**
     * sort words by percentage of vowels
     * @return sorted list
     */
    public List <String> sortByPercentage () {
        Pattern pat = Pattern.compile("[EYUOIAeyuioa]");

        List <String> result = this.splitIntoWords().stream().sorted((x,y)->{
            Matcher m = pat.matcher(x);
            double v = 0;
            while (m.find())v++;
            Double persX = v /x.length();
            v = 0;
            Matcher n = pat.matcher(y);
            while (n.find())v++;
            Double persY = v/y.length();
            return persX.compareTo(persY);
        }).collect(Collectors.toList());
        return result;
    }

    public <T> void printWords (Collection <T> collection){
        for (T i : collection) System.out.print ( " " + i);
    }

    /**
     * sort words witch start with vowels by next consonant
     * @return sorted list
     */
    public List <String> sortBySecondConsonant (){
        Pattern pat = Pattern.compile("^[EYUOIAeyuioa]\\w*");
        Pattern pat2 = Pattern.compile("[^EYUOIAeyuioa0-9]");
        List <String> result = this.splitIntoWords().stream().filter(x-> {
            Matcher m = pat.matcher(x);
            return m.matches();
        }).sorted((x,y)->{
            String compX = this.getFirstConsonant(x);
            String compY = this.getFirstConsonant(y);
            return compX.compareTo(compY);
                }).collect(Collectors.toList());
        return result;
    }

    private String getFirstConsonant (String word){
        Pattern pat = Pattern.compile("[^EYUOIAeyuioa0-9]");
        Matcher m = pat.matcher(word);
        if (m.find()) return m.group();
        else return " ";
    }

    /**
     * Sort words in text by quantity of given letter
     * @param letter given letter for sorting
     * @return sorted list
     */
    public List<String> sortByLetterNumber (String letter){
        Comparator <Word> comp = Comparator.comparing(Word::getWord);

        List <String> ansver = this.splitIntoWords().stream().map(i->{
            Word word = new Word(i);
            word.setCount(letter);
            return word;
        }).sorted(Comparator.comparing(Word::getCount).thenComparing(Word::getWord))
                .map(i->i.getWord()).collect(Collectors.toList());
        return ansver;
    }

}
