package regularExpression;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class StringUtils <T> {
    private List<String> parameters;
    private String strParam;

    public StringUtils ( T ...params){
        parameters = new ArrayList<>();
        strParam = "";
        for(T i: params){
            parameters.add(""+i);
            strParam+=(i + ", ");
        }
    }

    public void printParams (){
        System.out.println("The class " + this.getClass().getName()+ " has next parameters in constructor ");
        System.out.println(strParam);
    }
}
